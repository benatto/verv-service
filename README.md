# Coordinates Graph

This is one hour api test

## Build

To build the project

```
$ make build
```

## To start the project

```
$ make up

verv_1  |  * Serving Flask app "app" (lazy loading)
verv_1  |  * Environment: production
verv_1  |    WARNING: Do not use the development server in a production environment.
verv_1  |    Use a production WSGI server instead.
verv_1  |  * Debug mode: on
verv_1  |  * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
verv_1  |  * Restarting with stat
verv_1  |  * Debugger is active!
verv_1  |  * Debugger PIN: 212-344-307
```

## To run tests

```
make test

----------- coverage: platform linux, python 3.8.5-final-0 -----------
Name                         Stmts   Miss  Cover   Missing
----------------------------------------------------------
app.py                          16      2    88%   22, 26
project/resources/graph.py      16      2    88%   21-22
routes.py                        3      0   100%
tests/test_graph.py             10      0   100%
----------------------------------------------------------
TOTAL                           45      4    91%
```

## Example Coordinates for POST

POST `http://localhost:5000/api/v1/graph`

```
{
    "coordinates": [1, 2, 3, 45, 23, 1]
}
```

## Considerations

* 1 hour is not time enough to write an API in a good way
* The graph will be generated locally
* Business logic in the resources is not a good idea
* Better error handling