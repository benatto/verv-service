import json

from flask_restful import Resource
from flask import request

import matplotlib.pyplot as plt


class Graph(Resource):
    def post(self):
        if not request.data:
            return {"msg": "body not found"}, 400

        coordinates = json.loads(request.data).get("coordinates")

        # The business logic would be better in another layer
        # not in the handlers
        plt.plot(coordinates)
        plt.ylabel("Coordinates Example")
        plt.savefig("coordinates.png")

        try:
            return {"msg": "Coordinates received"}, 200
        except Exception:
            return {"error": "Some error here"}, 500
