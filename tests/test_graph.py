from app import app

app.testing = True
app.debug = True


class TestGraph(object):
    def test_coordinates_should_return_200(self):
        # Act
        response = app.test_client().post(
            "/api/v1/graph", json={"coordinates": [1, 2, 3]}
        )

        # Assert
        assert response.status_code == 200

    def test_coordinates_without_body_should_return_400(self):
        # Act
        response = app.test_client().post("/api/v1/graph")

        # Assert
        assert response.status_code == 400

    def test_coordinates_not_allowed_method_should_return_405(self):
        # Act
        response = app.test_client().get("/api/v1/graph")

        # Assert
        assert response.status_code == 405

    def test_coordinates_invalid_route_should_return_404(self):
        # Act
        response = app.test_client().post("/api/v1/foo")

        # Assert
        assert response.status_code == 404
