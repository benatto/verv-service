clean:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +
	find . -name '.pytest_cache' -exec rm -fr {} +
	find . -name '.mypy_cache' -exec rm -fr {} +

lint:
	docker-compose -f docker-compose.testing.yml run verv flake8 --ignore=E501,E266 src tests

test:
	docker-compose -f docker-compose.testing.yml run verv python3 -m pytest --cov-report term-missing --cov=/app -s -v tests/

mypy:
	docker-compose -f docker-compose.testing.yml run verv mypy --ignore-missing-imports .

build:
	docker-compose -f docker-compose.yml build

up:
	docker-compose up
