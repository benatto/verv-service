from project.resources.graph import Graph


def add(api):
    # Add all endpoints here
    api.add_resource(Graph, "/api/v1/graph")
